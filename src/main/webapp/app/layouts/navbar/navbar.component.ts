import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiLanguageService } from 'ng-jhipster';
import { SessionStorageService } from 'ngx-webstorage';

import { VERSION } from 'app/app.constants';
import { JhiLanguageHelper, AccountService, LoginModalService, LoginService } from 'app/core';
import { ProfileService } from '../profiles/profile.service';
import { routes } from './toolbar-nav.routes';

@Component({
    selector: 'jhi-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['navbar.scss']
})
export class NavbarComponent implements OnInit {
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    showSuggestions = false;
    suggestions: any[];
    navRoutes = routes;
    navInput: string;
    @Input() leftMenuHidden: boolean;
    @Output() toggleBtnLeftMenuEvent = new EventEmitter<boolean>();
    @Output() onLogout = new EventEmitter<boolean>();
    @HostListener('document:click', ['$event'])
    documentClick(event: MouseEvent) {
        this.showSuggestions = false;
    }

    constructor(
        private loginService: LoginService,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private sessionStorage: SessionStorageService,
        private accountService: AccountService,
        private loginModalService: LoginModalService,
        private profileService: ProfileService,
        private router: Router
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
    }

    ngOnInit() {
        this.languageHelper.getAll().then(languages => {
            this.languages = languages;
        });

        this.profileService.getProfileInfo().then(profileInfo => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });
    }

    toggleLeftMenu() {
        this.leftMenuHidden = !this.leftMenuHidden;
        this.toggleBtnLeftMenuEvent.emit(this.leftMenuHidden);
    }

    changeLanguage(languageKey: string) {
        this.sessionStorage.store('locale', languageKey);
        this.languageService.changeLanguage(languageKey);
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.onLogout.emit();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.accountService.getImageUrl() : null;
    }

    onChange(event) {
        const newSuggestions = [];
        for (let i = 0; i < this.navRoutes.length; i += 1) {
            if (this.navRoutes[i].name.toUpperCase().indexOf(event.toUpperCase()) !== -1) {
                newSuggestions.push(this.navRoutes[i]);
            }
        }
        setTimeout(() => {
            this.suggestions = newSuggestions.slice(0, 3);
            if (this.suggestions.length > 0) {
                this.showSuggestions = true;
            } else {
                this.showSuggestions = false;
            }
        }, 10);
    }
}
