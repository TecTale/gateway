export const routes = [
    {
        name: 'Home',
        routerLink: '/dashboard'
    },
    {
        name: 'Settings',
        routerLink: '/settings'
    },
    {
        name: 'Password',
        routerLink: '/password'
    },
    {
        name: 'Search',
        routerLink: '/search'
    },
    {
        name: 'Adjust',
        routerLink: '/adjust'
    },
    {
        name: 'Move',
        routerLink: '/move'
    },
    {
        name: 'Change',
        routerLink: '/change'
    },
    {
        name: 'Adjust Wizard',
        routerLink: '/adjust/wizard'
    }
];
