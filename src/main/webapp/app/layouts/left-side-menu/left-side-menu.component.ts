import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { AccountService } from 'app/core';

@Component({
    selector: 'jhi-left-side-menu',
    templateUrl: './left-side-menu.component.html',
    styles: ['./left-side-menu.scss']
})
export class LeftSideMenuComponent implements OnInit {
    @Input() leftMenuHidden: boolean;
    @Output() elementClicked = new EventEmitter<boolean>();
    showInventory = false;
    showAdmin = false;

    constructor(private accountService: AccountService, private location: Location) {
        this.leftMenuHidden = false;
    }

    ngOnInit() {}

    toggleInventory() {
        this.showInventory = !this.showInventory;
    }

    toggleAdmin() {
        this.showAdmin = !this.showAdmin;
    }

    onNavigate() {
        this.elementClicked.emit(true);
        console.log(this.location.path());
    }
}
