import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import { ChangeComponent, changeRoute } from './';

const ENTITY_STATES = [...changeRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [ChangeComponent],
    entryComponents: [ChangeComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayChangeModule {}
