import { ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { ChangeComponent } from './change.component';

export class ChangeResolve {
    constructor() {}
}

export const changeRoute: Routes = [
    {
        path: 'change',
        component: ChangeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.change.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
