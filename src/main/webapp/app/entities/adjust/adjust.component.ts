import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { SampleService } from './adjust.service';

@Component({
    selector: 'jhi-adjust',
    templateUrl: './adjust.component.html'
})
export class AdjustComponent implements OnInit {
    stage = 1;
    serialInputs = [];
    stageOneForm: FormGroup;
    stageTwoForm: FormGroup;
    stageThreeForm: FormGroup;
    stageFourForm: FormGroup;
    isSer = false;
    requestSuccess = true;

    constructor(
        private fb: FormBuilder,
        private toastr: ToastrService,
        private route: ActivatedRoute,
        private sampleService: SampleService
    ) {}

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (params.active) {
                this.stageOneForm = this.fb.group({
                    orgNum: [params.orgNum || '', Validators.required],
                    facilityNum: [params.facilityNum || '', Validators.required],
                    locationNum: [params.locationNum || '', Validators.required],
                    itemNum: [params.itemNum || '', Validators.required],
                    lpnNum: [params.lpnNum || ''],
                    lpnType: [params.lpnType.toLowerCase() || ''],
                    productClass: [params.productClass || ''],
                    uom: [params.uom.toLowerCase() || '', Validators.required],
                    inventoryStatus: [params.inventoryStatus.toLowerCase() || '', Validators.required]
                });
                this.stageTwoForm = this.fb.group({
                    shipmentNum: [params.shipmentNum || ''],
                    receiptDate: [params.receiptDate || ''],
                    manufacturedDate: [params.manufacturedDate || ''],
                    shipByDate: [params.shipByDate || ''],
                    segmentNum: [params.segmentNum || ''],
                    segmentType: [params.segmentType.toLowerCase() || ''],
                    countryOfOrigin: [params.countryOfOrigin || '']
                });
                this.stageThreeForm = this.fb.group({
                    batchNum: [params.batchNum || ''],
                    lotNum: [params.lotNum || ''],
                    revisionNum: [params.revisionNum || ''],
                    inventoryAttribute1: [params.inventoryAttribute1 || ''],
                    inventoryAttribute2: [params.inventoryAttribute2 || ''],
                    inventoryAttribute3: [params.inventoryAttribute3 || ''],
                    inventoryAttribute4: [params.inventoryAttribute4 || ''],
                    inventoryAttribute5: [params.inventoryAttribute5 || ''],
                    quantity: [1, Validators.min(1)],
                    operation: ['add']
                });
            } else {
                this.stageOneForm = this.fb.group({
                    orgNum: [params.orgNum || '', Validators.required],
                    facilityNum: ['', Validators.required],
                    locationNum: ['', Validators.required],
                    itemNum: ['', Validators.required],
                    lpnNum: [''],
                    lpnType: [''],
                    productClass: [''],
                    uom: ['', Validators.required],
                    inventoryStatus: ['', Validators.required]
                });
                this.stageTwoForm = this.fb.group({
                    shipmentNum: [''],
                    receiptDate: [''],
                    manufacturedDate: [''],
                    shipByDate: [''],
                    segmentNum: [''],
                    segmentType: [''],
                    countryOfOrigin: ['']
                });
                this.stageThreeForm = this.fb.group({
                    batchNum: [''],
                    lotNum: [''],
                    revisionNum: [''],
                    inventoryAttribute1: [''],
                    inventoryAttribute2: [''],
                    inventoryAttribute3: [''],
                    inventoryAttribute4: [''],
                    inventoryAttribute5: [''],
                    quantity: [1, Validators.min(1)],
                    operation: ['add']
                });
            }
        });
        this.stageFourForm = this.fb.group({
            auditCode: [''],
            auditDescription: ['']
        });
        this.stageThreeForm.controls['inventoryAttribute1'].disable();
        this.stageThreeForm.controls['inventoryAttribute2'].disable();
        this.stageThreeForm.controls['inventoryAttribute3'].disable();
        this.stageThreeForm.controls['inventoryAttribute4'].disable();
        this.stageThreeForm.controls['inventoryAttribute5'].disable();
    }

    newSerialInput() {
        this.serialInputs.push({ serialId: '', secSer1: '', secSer2: '', secSer3: '', secSer4: '' });
    }

    deleteSerialInput(index) {
        this.serialInputs.splice(index, 1);
    }

    moveToStage(stage, link) {
        if (stage < this.stage || !link) {
            if (stage === 1) {
                this.stageThreeForm.controls['batchNum'].enable();
                this.stageThreeForm.controls['revisionNum'].enable();
                this.stageThreeForm.controls['lotNum'].enable();
                this.stageThreeForm.value.quantity = 1;
                this.isSer = false;

                this.stage = stage;
            }
            if (stage === 2) {
                if (this.stageOneForm.value.itemNum.toLowerCase().indexOf('b') === -1) {
                    this.stageThreeForm.controls['batchNum'].disable();
                }
                if (this.stageOneForm.value.itemNum.toLowerCase().indexOf('r') === -1) {
                    this.stageThreeForm.controls['revisionNum'].disable();
                }
                if (this.stageOneForm.value.itemNum.toLowerCase().indexOf('l') === -1) {
                    this.stageThreeForm.controls['lotNum'].disable();
                }
                if (this.stageOneForm.value.itemNum.toLowerCase().indexOf('s') !== -1) {
                    this.isSer = true;
                    this.stageThreeForm.controls['quantity'].disable();
                }

                this.stage = stage;
            }
            if (stage === 3) {
                this.stage = stage;
            }
            if (stage === 4) {
                this.stage = stage;
            }
        }
    }

    onSubmit() {
        const body = Object.assign(
            {},
            this.stageOneForm.value,
            this.stageTwoForm,
            this.stageThreeForm,
            this.stageFourForm,
            this.serialInputs
        );
        console.log(body);

        const success = this.sampleService.samplePost(body);
        if (success) {
            // if no errors are returned from the server run the following Code to reset the form
            this.stageOneForm.reset();
            this.stageTwoForm.reset();
            this.stageThreeForm.reset();
            this.stageFourForm.reset();
            this.stageOneForm.controls['inventoryStatus'].setValue('', { onlySelf: true });
            this.stageOneForm.controls['uom'].setValue('', { onlySelf: true });
            this.stageOneForm.controls['lpnType'].setValue('', { onlySelf: true });
            this.stageTwoForm.controls['segmentType'].setValue('', { onlySelf: true });
            this.stageThreeForm.controls['operation'].setValue('add', { onlySelf: true });
            this.serialInputs = [];

            // disable all conditional inputs
            this.stageThreeForm.controls['batchNum'].disable();
            this.stageThreeForm.controls['revisionNum'].disable();
            this.stageThreeForm.controls['lotNum'].disable();
            this.stageThreeForm.controls['quantity'].disable();

            this.stage = 1;
            const scrollToTop = window.setInterval(() => {
                const pos = window.pageYOffset;
                if (pos > 0) {
                    window.scrollTo(0, pos - 20);
                } else {
                    window.clearInterval(scrollToTop);
                }
            }, 16);
            // 'Sucessful update, http response-- 201'
            // 'Successfully created item, HTTP response - 201'

            this.toastr.success('success message', 'Success!');
        } else {
            // 'Invalid data passed in JSON, http response - 400'
            // 'Invalid operation / endpoint, http response - 404'
            // 'Field validation error, http response - 400'
            // 'API error, http response -- 500'

            this.toastr.error('error message', 'Error!');
        }
    }
}
