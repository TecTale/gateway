import { ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { AdjustComponent } from './adjust.component';

export class AdjustResolve {
    constructor() {}
}

export const adjustRoute: Routes = [
    {
        path: 'adjust',
        component: AdjustComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.adjust.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
