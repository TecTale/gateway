import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import { AdjustComponent, adjustRoute } from './';

const ENTITY_STATES = [...adjustRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [AdjustComponent],
    entryComponents: [AdjustComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayAdjustModule {}
