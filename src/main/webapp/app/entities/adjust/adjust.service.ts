import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const url = '';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};
@Injectable({
    providedIn: 'root'
})
export class SampleService {
    constructor(private http: HttpClient) {}

    samplePost(body: string) {
        // return this.http.post<SearchRes>(url, body, httpOptions);
        return true;
    }
}
