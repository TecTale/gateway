import { ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { ChangeWizComponent } from './changewiz.component';

export class ChangeWizResolve {
    constructor() {}
}

export const changewizRoute: Routes = [
    {
        path: 'change/wizard',
        component: ChangeWizComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.adjust.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
