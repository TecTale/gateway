import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import { ChangeWizComponent, changewizRoute } from './';

const ENTITY_STATES = [...changewizRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [ChangeWizComponent],
    entryComponents: [ChangeWizComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayChangeWizModule {}
