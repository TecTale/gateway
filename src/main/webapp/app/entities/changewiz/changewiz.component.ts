import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'jhi-change-wiz',
    templateUrl: './changewiz.component.html'
})
export class ChangeWizComponent implements OnInit {
    stage = 1;
    serialInputs = [];
    stageOneForm: FormGroup;
    stageTwoForm: FormGroup;
    stageThreeForm: FormGroup;
    stageFourForm: FormGroup;
    isSer = false;

    constructor(private fb: FormBuilder, private toastr: ToastrService, private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (params.active) {
                this.stageOneForm = this.fb.group({
                    orgNum: [params.orgNum || '', Validators.required],
                    facilityNum: [params.facilityNum || '', Validators.required],
                    locationNum: [params.locationNum || '', Validators.required],
                    itemNum: [params.itemNum || '', Validators.required],
                    lpnNum: [params.lpnNum || ''],
                    lpnType: [params.lpnType.toLowerCase() || ''],
                    productClass: [params.productClass || ''],
                    uom: [params.uom.toLowerCase() || '', Validators.required],
                    inventoryStatus: [params.inventoryStatus.toLowerCase() || '', Validators.required]
                });
                this.stageTwoForm = this.fb.group({
                    batchNum: [params.batchNum || ''],
                    lotNum: [params.lotNum || ''],
                    revisionNum: [params.revisionNum || ''],
                    inventoryAttribute1: [params.inventoryAttribute1 || ''],
                    inventoryAttribute2: [params.inventoryAttribute2 || ''],
                    inventoryAttribute3: [params.inventoryAttribute3 || ''],
                    inventoryAttribute4: [params.inventoryAttribute4 || ''],
                    inventoryAttribute5: [params.inventoryAttribute5 || '']
                });
                this.stageThreeForm = this.fb.group({
                    shipmentNum: [params.shipmentNum || ''],
                    receiptDate: [params.receiptDate || ''],
                    manufacturedDate: [params.manufacturedDate || ''],
                    shipByDate: [params.shipByDate || ''],
                    segmentNum: [params.segmentNum || ''],
                    segmentType: [params.segmentType.toLowerCase() || ''],
                    countryOfOrigin: [params.countryOfOrigin || ''],
                    quantity: [1, Validators.min(1)],
                    shipmentNumTarget: [''],
                    receiptDateTarget: [''],
                    manufacturedDateTarget: [''],
                    shipByDateTarget: [''],
                    countryOfOriginTarget: [''],
                    segmentNumTarget: [''],
                    segmentTypeTarget: [''],
                    quantityTarget: [1, Validators.min(1)]
                });
            } else {
                this.stageOneForm = this.fb.group({
                    orgNum: ['', Validators.required],
                    facilityNum: ['', Validators.required],
                    locationNum: ['', Validators.required],
                    itemNum: ['', Validators.required],
                    lpnNum: [''],
                    lpnType: [''],
                    productClass: [''],
                    uom: ['', Validators.required],
                    inventoryStatus: ['', Validators.required]
                });
                this.stageTwoForm = this.fb.group({
                    batchNum: [''],
                    lotNum: [''],
                    revisionNum: [''],
                    inventoryAttribute1: [''],
                    inventoryAttribute2: [''],
                    inventoryAttribute3: [''],
                    inventoryAttribute4: [''],
                    inventoryAttribute5: ['']
                });
                this.stageThreeForm = this.fb.group({
                    shipmentNum: [''],
                    receiptDate: [''],
                    manufacturedDate: [''],
                    shipByDate: [''],
                    segmentNum: [''],
                    segmentType: [''],
                    countryOfOrigin: [''],
                    quantity: [1, Validators.min(1)],
                    shipmentNumTarget: [''],
                    receiptDateTarget: [''],
                    manufacturedDateTarget: [''],
                    shipByDateTarget: [''],
                    countryOfOriginTarget: [''],
                    segmentNumTarget: [''],
                    segmentTypeTarget: [''],
                    quantityTarget: [1, Validators.min(1)]
                });
            }
        });
        this.stageFourForm = this.fb.group({
            auditCode: [''],
            auditDescription: ['']
        });
        this.stageTwoForm.controls['inventoryAttribute1'].disable();
        this.stageTwoForm.controls['inventoryAttribute2'].disable();
        this.stageTwoForm.controls['inventoryAttribute3'].disable();
        this.stageTwoForm.controls['inventoryAttribute4'].disable();
        this.stageTwoForm.controls['inventoryAttribute5'].disable();
    }

    newSerialInput() {
        this.serialInputs.push({ serialId: '', secSer1: '', secSer2: '', secSer3: '', secSer4: '' });
    }

    deleteSerialInput(index) {
        this.serialInputs.splice(index, 1);
    }

    moveToStage(stage, link) {
        if (stage < this.stage || !link) {
            if (stage === 1) {
                this.stageTwoForm.controls['batchNum'].enable();
                this.stageTwoForm.controls['revisionNum'].enable();
                this.stageTwoForm.controls['lotNum'].enable();
                this.isSer = false;

                this.stage = stage;
            }
            if (stage === 2) {
                if (this.stageOneForm.value.itemNum.toLowerCase().indexOf('b') === -1) {
                    this.stageTwoForm.controls['batchNum'].disable();
                }
                if (this.stageOneForm.value.itemNum.toLowerCase().indexOf('r') === -1) {
                    this.stageTwoForm.controls['revisionNum'].disable();
                }
                if (this.stageOneForm.value.itemNum.toLowerCase().indexOf('l') === -1) {
                    this.stageTwoForm.controls['lotNum'].disable();
                }
                if (this.stageOneForm.value.itemNum.toLowerCase().indexOf('s') !== -1) {
                    this.isSer = true;
                    this.stageThreeForm.controls['quantity'].disable();
                }

                this.stage = stage;
            }
            if (stage === 3) {
                this.stage = stage;
            }
            if (stage === 4) {
                this.stage = stage;
            }
        }
    }

    onSubmit() {
        // if no errors are returned from the server run the following Code to reset the form
        this.stageOneForm.reset();
        this.stageTwoForm.reset();
        this.stageThreeForm.reset();
        this.stageFourForm.reset();
        this.stageOneForm.controls['inventoryStatus'].setValue('', { onlySelf: true });
        this.stageOneForm.controls['uom'].setValue('', { onlySelf: true });
        this.stageOneForm.controls['lpnType'].setValue('', { onlySelf: true });
        this.stageThreeForm.controls['segmentType'].setValue('', { onlySelf: true });
        this.serialInputs = [];

        // disable all conditional inputs
        this.stageTwoForm.controls['batchNum'].disable();
        this.stageTwoForm.controls['revisionNum'].disable();
        this.stageTwoForm.controls['lotNum'].disable();
        this.stageThreeForm.controls['quantity'].disable();

        this.stage = 1;

        this.toastr.success('Your request to the server was returned successfully', 'Success!');

        // this.toastr.error('error message', 'Error!');
    }
}
