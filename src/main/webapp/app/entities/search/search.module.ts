import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import { SearchComponent, SearchDialogComponent, searchRoute } from './';

const ENTITY_STATES = [...searchRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [SearchComponent, SearchDialogComponent],
    entryComponents: [SearchComponent, SearchDialogComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewaySearchModule {}
