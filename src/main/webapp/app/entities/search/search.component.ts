import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { SearchService } from './search.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Product } from '../../shared/model/product.model';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { MatDialog } from '@angular/material';
import { SearchDialogComponent } from './search.dialog';
import { SlideInOutAnimation } from '../../shared/animations/slide.animation';
import { Router } from '@angular/router';

@Component({
    selector: 'jhi-search',
    templateUrl: './search.component.html',
    animations: [SlideInOutAnimation]
})
export class SearchComponent implements OnInit {
    basicSearchInput = '';
    showFilters = false;
    showAdvancedForm = false;
    products: any[];
    advancedSearchForm: FormGroup;
    filters: object[];
    highlightList = [];
    currentDetail: number;
    animationState = 'out';
    currentPage = 0;
    lastTerm: any = '';
    lastSearch = 'basic';
    @HostListener('scroll', ['$event'])
    onScroll(event: any) {
        if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight) {
            if (this.lastSearch === 'basic') {
                this.searchService.basicSearch(this.lastTerm, this.currentPage).subscribe(data => {
                    this.products = this.products.concat(data.hits.hits);
                    this.currentPage += 200;
                });
            }
            if (this.lastSearch === 'advanced') {
                this.searchService.advancedSearch(this.lastTerm, this.currentPage).subscribe(data => {
                    this.products = this.products.concat(data.hits.hits);
                    this.currentPage += 200;
                });
            }
        }
    }

    constructor(
        @Inject(LOCAL_STORAGE) private storage: WebStorageService,
        private searchService: SearchService,
        private fb: FormBuilder,
        private dialog: MatDialog,
        private router: Router
    ) {}

    ngOnInit() {
        this.searchService.basicSearch('', this.currentPage).subscribe(data => {
            this.products = data.hits.hits;
            this.currentPage += 200;
        });

        this.createForm();

        this.filters = this.storage.get('filters');

        if (!this.filters) {
            this.storage.set('filters', []);
        }

        console.log(this.filters);
    }

    createForm() {
        this.advancedSearchForm = this.fb.group({
            itemNum: [''],
            productClass: [''],
            locationNum: [''],
            zoneNum: [''],
            lpnNum: [''],
            batchNum: [''],
            countryOfOrigin: [''],
            segmentNum: [''],
            serialNum: [''],
            revisionNum: ['']
        });
    }

    onChange(event) {
        this.basicSearchInput = event.target.value;
    }

    basicSearch() {
        const term = this.basicSearchInput;
        this.currentPage = 0;
        this.searchService.basicSearch(term, this.currentPage).subscribe(data => {
            this.products = data.hits.hits;
            this.currentPage += 200;
            this.lastSearch = 'basic';
            this.lastTerm = term;
        });
    }

    toggleAdvanced() {
        this.animationState = this.animationState === 'out' ? 'in' : 'out';
        this.showFilters = false;
    }

    toggleFilters() {
        this.showFilters = !this.showFilters;
    }

    onSubmit() {
        const term = this.advancedSearchForm.value;
        this.currentPage = 0;
        this.searchService.advancedSearch(this.advancedSearchForm.value, this.currentPage).subscribe(data => {
            this.products = data.hits.hits;
            this.currentPage += 200;
            this.lastSearch = 'advanced';
            this.lastTerm = term;
        });
        this.animationState = 'out';
        this.advancedSearchForm.reset();
    }

    openDialog() {
        const dialogRef = this.dialog.open(SearchDialogComponent, { width: '250px' });

        dialogRef.afterClosed().subscribe(result => {
            const newFilter = { terms: {}, name: '' };
            newFilter.terms = this.advancedSearchForm.value;
            newFilter.name = result;
            if (result === '') {
                newFilter.name = 'Filter';
            }
            this.filters.push(newFilter);
            console.log(this.filters);
            this.storage.set('filters', this.filters);
            this.advancedSearchForm.reset();
        });
    }

    selectFilter(terms) {
        this.searchService.advancedSearch(terms, 0).subscribe(data => {
            this.products = data.hits.hits;
        });
        this.animationState = 'out';
    }

    deleteFilter(index) {
        this.filters.splice(index, 1);
        this.storage.set('filters', this.filters);
    }

    selectItem(number) {
        const index = this.highlightList.indexOf(number);
        if (index !== -1) {
            this.highlightList.splice(index, 1);
        } else {
            this.highlightList.push(number);
        }
    }

    toggleDetails(event, index) {
        event.stopPropagation();

        if (this.currentDetail === index) {
            this.currentDetail = null;
        } else {
            this.currentDetail = index;
        }
    }

    moveWithParams(event, path, product) {
        event.stopPropagation();
        this.router.navigate([path], {
            queryParams: {
                orgNum: product.orgNum || '',
                facilityNum: product.facilityNum || '',
                locationNum: product.locationNum || '',
                lpnType: product.lpnType || '',
                lpnNum: product.lpnNum || '',
                itemNum: product.itemNum || '',
                uom: product.uom || '',
                productClass: product.productClass || '',
                inventoryStatus: product.inventoryStatus || '',
                quantity: product.quantity || 1,
                lotNum: product.lotNum || '',
                batchNum: product.batchNum || '',
                shipmentNum: product.shipmentNum || '',
                receiptDate: product.receiptDate || '',
                shipByDate: product.receiptDate || '',
                segmentType: product.segmentType || '',
                segmentNum: product.segmentNum || '',
                manufacturedDate: product.manufacturedDate || '',
                inventoryAttribute1: product.inventoryAttribute1 || '',
                inventoryAttribute2: product.inventoryAttribute2 || '',
                inventoryAttribute3: product.inventoryAttribute3 || '',
                inventoryAttribute4: product.inventoryAttribute4 || '',
                inventoryAttribute5: product.inventoryAttribute5 || '',
                countryOfOrigin: product.countryOfOrigin || '',
                revisionNum: product.revisionNum || '',
                active: true
            }
        });
    }
}
