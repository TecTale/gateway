import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SearchRes } from '../../shared/model/searchres.model';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};
@Injectable({
    providedIn: 'root'
})
export class SearchService {
    constructor(private http: HttpClient) {}

    basicSearch(term: string, page: number) {
        const body = {
            query: {
                query_string: {
                    query: `*${term}*`,
                    fields: ['itemNum', 'batchNum', 'locationNum']
                }
            },
            from: page,
            size: 200
        };

        return this.http.post<SearchRes>('http://localhost:9200/_search', body, httpOptions);
    }

    advancedSearch(terms: object, page: number) {
        const toArray = Object.keys(terms).map(i => {
            if (terms[i] !== '') {
                const obj = { term: {} };
                obj.term[i] = terms[i].toLowerCase();
                return obj;
            }
        });
        const query = toArray.filter(e => e !== undefined);
        const body = {
            query: {
                bool: {
                    must: query
                }
            },
            from: page,
            size: 200
        };
        console.log(body.query.bool.must);
        return this.http.post<SearchRes>('http://localhost:9200/_search', body, httpOptions);
    }
}
