import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { SearchComponent } from './search.component';

@Component({
    selector: 'jhi-filter-dialog',
    templateUrl: './search.dialog.html'
})
export class SearchDialogComponent implements OnInit {
    data = '';

    constructor(public dialogRef: MatDialogRef<SearchComponent>) {}

    ngOnInit() {}

    onNoClick(): void {
        this.dialogRef.close();
    }
}
