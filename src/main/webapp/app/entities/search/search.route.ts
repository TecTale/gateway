import { ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { SearchComponent } from './search.component';

export class SearchResolve {
    constructor() {}
}

export const searchRoute: Routes = [
    {
        path: 'search',
        component: SearchComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.search.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
