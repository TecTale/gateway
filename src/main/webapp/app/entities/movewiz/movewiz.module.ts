import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import { MoveWizComponent, movewizRoute } from './';

const ENTITY_STATES = [...movewizRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [MoveWizComponent],
    entryComponents: [MoveWizComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayMoveWizModule {}
