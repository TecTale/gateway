import { ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { MoveWizComponent } from './movewiz.component';

export class MoveWizResolve {
    constructor() {}
}

export const movewizRoute: Routes = [
    {
        path: 'move/wizard',
        component: MoveWizComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.adjust.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
