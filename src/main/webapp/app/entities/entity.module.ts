import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { GatewaySearchModule } from './search/search.module';
import { GatewayMoveModule } from './move/move.module';
import { GatewayMoveWizModule } from './movewiz/movewiz.module';
import { GatewayChangeModule } from './change/change.module';
import { GatewayChangeWizModule } from './changewiz/changewiz.module';
import { GatewayAdjustModule } from './adjust/adjust.module';
import { GatewayAdjustWizModule } from './adjustwiz/adjustwiz.module';
import { GatewayDashboardModule } from './dashboard/dashboard.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        GatewaySearchModule,
        GatewayMoveModule,
        GatewayChangeModule,
        GatewayChangeWizModule,
        GatewayAdjustModule,
        GatewayAdjustWizModule,
        GatewayDashboardModule,
        GatewayMoveWizModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayEntityModule {}
