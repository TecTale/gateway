import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import { MoveComponent, moveRoute } from './';

const ENTITY_STATES = [...moveRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [MoveComponent],
    entryComponents: [MoveComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayMoveModule {}
