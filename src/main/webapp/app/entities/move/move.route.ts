import { ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { MoveComponent } from './move.component';

export class MoveResolve {
    constructor() {}
}

export const moveRoute: Routes = [
    {
        path: 'move',
        component: MoveComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.move.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
