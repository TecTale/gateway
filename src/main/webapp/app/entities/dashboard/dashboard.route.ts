import { ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { DashboardComponent } from './dashboard.component';

export class DashboardResolve {
    constructor() {}
}

export const dashboardRoute: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.move.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
