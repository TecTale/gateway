import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import { DashboardComponent, dashboardRoute } from './';

const ENTITY_STATES = [...dashboardRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [DashboardComponent],
    entryComponents: [DashboardComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayDashboardModule {}
