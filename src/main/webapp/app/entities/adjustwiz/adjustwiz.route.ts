import { ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { AdjustWizComponent } from './adjustwiz.component';

export class AdjustWizResolve {
    constructor() {}
}

export const adjustwizRoute: Routes = [
    {
        path: 'adjust/wizard',
        component: AdjustWizComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.adjust.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
