import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import { AdjustWizComponent, adjustwizRoute } from './';

const ENTITY_STATES = [...adjustwizRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [AdjustWizComponent],
    entryComponents: [AdjustWizComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayAdjustWizModule {}
