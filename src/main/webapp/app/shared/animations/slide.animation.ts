import { trigger, state, style, transition, animate, group, query, stagger, keyframes } from '@angular/animations';

export const SlideInOutAnimation = [
    trigger('slideInOut', [
        state(
            'in',
            style({
                'margin-top': '-60px',
                opacity: '1'
            })
        ),
        state(
            'out',
            style({
                'margin-top': '-600px',
                opacity: '0'
            })
        ),
        transition('in => out', [
            group([
                animate(
                    '500ms ease-in-out',
                    style({
                        opacity: '0'
                    })
                ),
                animate(
                    '600ms ease-in-out',
                    style({
                        'margin-top': '-600px'
                    })
                )
            ])
        ]),
        transition('out => in', [
            group([
                animate(
                    '600ms ease-in-out',
                    style({
                        'margin-top': '-60px'
                    })
                ),
                animate(
                    '500ms ease-in-out',
                    style({
                        opacity: '1'
                    })
                )
            ])
        ])
    ])
];
