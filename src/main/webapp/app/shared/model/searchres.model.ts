import { Product } from './product.model';

export class SearchRes {
    took: number;
    timed_out: boolean;
    _shards: {
        total: number;
        successful: number;
        skipped: number;
        failed: number;
    };
    hits: {
        total: number;
        max_score: number;
        hits: Product[];
    };
}
