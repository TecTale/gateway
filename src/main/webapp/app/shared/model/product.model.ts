import { Serial } from './serial.model';

export class Product {
    _index: string;
    _type: string;
    _id: string;
    _score: number;
    _source: {
        orgNum: string;
        facilityNum: string;
        locationNum: string;
        zoneNum: string;
        lpnType: string;
        parentLpnNum: string;
        itemNum: string;
        uom: string;
        productClass: string;
        inventoryStatus: string;
        moveInQty: number;
        moveOutQty: number;
        quantity: number;
        lotNum: string;
        batchNum: string;
        shipmentNum: string;
        receiptDate: string;
        shipByDate: string;
        segmentType: string;
        segmentNum: string;
        manufacturedDate: string;
        inventoryAttribute1: string;
        countryOfOrigin: string;
        revisionNum: string;
        hasNonAllocatableLock: boolean;
        hasNonPromisableLock: boolean;
        serials: Serial[];
    };
}
