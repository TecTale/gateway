export class Serial {
    serialId: string;
    serialNum: string;
    secondarySerial1: string;
    secondarySerial2: string;
}
