package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Search;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Search entity.
 */
public interface SearchSearchRepository extends ElasticsearchRepository<Search, Long> {
}
